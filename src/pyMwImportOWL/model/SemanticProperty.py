'''
Created on 10.05.2016

@author: Alvaro.Ortiz
'''
class SemanticProperty:
    
    def __init__(self, name):
        self.name = name
        self.type = None
        self.allowedValues = None
